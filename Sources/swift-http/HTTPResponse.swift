//
//  HTTPResponse.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public struct HTTPResponse: HTTPMessage {
    public var version: HTTPVersion
    public var status: HTTPStatus

    public var headers: [String : String] = [:]

    public var message: String?

    init(version: HTTPVersion, status: HTTPStatus) {
        self.version = version
        self.status = status
    }

    public static func ok() -> HTTPResponse {
        return HTTPResponse(version: HTTPVersion.V1_1, status: HTTPStatus.code_200)
    }

    public static func badRequest() -> HTTPResponse {
        return HTTPResponse(version: HTTPVersion.V1_1, status: HTTPStatus.code_400)
    }

    public static func notFound() -> HTTPResponse {
        return HTTPResponse(version: HTTPVersion.V1_1, status: HTTPStatus.code_404)
    }

    public static func internalError(error: Error? = nil) -> HTTPResponse {
        var response = HTTPResponse(version: HTTPVersion.V1_1, status: HTTPStatus.code_500)
        if let error = error {
            response.message = error.localizedDescription
        }
        return response
    }
}

public extension HTTPResponse {
    var Connection: String? {
        get {
            return headers["Connection"]
        }

        set {
            headers["Connection"] = newValue
        }
    }
}

public extension HTTPResponse {
    var ContentLength: Int? {
        get {
            return headers["Content-Length"].flatMap { Int($0) }
        }

        set {
            headers["Content-Length"] = newValue.map { $0.description }
        }
    }

    var ContentType: String? {
        get {
            return headers["Content-Type"]
        }

        set {
            headers["Content-Type"] = newValue
        }
    }
}

public extension HTTPResponse {
    var Age: String? {
        get {
            return headers["Age"]
        }
        set {
            headers["Age"] = newValue
        }
    }

    var Location: String? {
        return headers["Location"]
    }

    var ProxyAuthenticate: String? {
        return headers["Proxy-Authenticate"]
    }

    var Public: String? {
        return headers["Public"]
    }

    var RetryAfter: String? {
        return headers["Retry-After"]
    }

    var Server: String? {
        return headers["Server"]
    }

    var Vary: String? {
        return headers["Vary"]
    }

    var Warning: String? {
        return headers["Warning"]
    }

    var WWWAuthenticate: String? {
        return headers["WWW-Authenticate"]
    }
}
