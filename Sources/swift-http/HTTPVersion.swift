//
//  HTTPVersion.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public enum HTTPVersion: String {
    case V1_0 = "HTTP/1.0"
    case V1_1 = "HTTP/1.1"
    case V2_0 = "HTTP/2.0"

    public static let dataV1_0 = V1_0.rawValue.data(using: String.Encoding.utf8)!
    public static let dataV1_1 = V1_1.rawValue.data(using: String.Encoding.utf8)!
    public static let dataV2_0 = V2_0.rawValue.data(using: String.Encoding.utf8)!

    var data: Data {
        switch self {
        case .V1_0:
            return HTTPVersion.dataV1_0
        case .V1_1:
            return HTTPVersion.dataV1_1
        case .V2_0:
            return HTTPVersion.dataV2_0
        }
    }
}
