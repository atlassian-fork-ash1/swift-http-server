//
//  HTTPMethod.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public enum HTTPMethod: String {
    case OPTIONS = "OPTIONS"
    case GET = "GET"
    case HEAD = "HEAD"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
    case TRACE = "TRACE"
}

